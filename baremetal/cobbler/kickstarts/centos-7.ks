#version=RHEL7
# System authorization information
auth --enableshadow --passalgo=sha512
# Use network installation
url --url="$tree"
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8
# System bootloader configuration
#set boot = $getVar('boot', 'sda')
bootloader --location=mbr --boot-drive=$boot
# Root password
rootpw --iscrypted $1$KBqYGueB$nM21gsYOV0wQQHPMD5Mr31
# System services
services --enabled="chronyd"
# System timezone
timezone US/Pacific --isUtc
# Network information
#set mgmt = $getVar('mgmt', 'eth0')
network  --bootproto=static --ip=$ip --netmask=$mask --gateway=$gateway --device=$mgmt --ipv6=auto --nameserver=$name_servers[0] --activate --hostname=$name.$name_servers_search[0]
# Partition clearing information
#set ospart = $getVar('ospart', 'sda')
clearpart --all --initlabel --drives=$ospart
ignoredisk --only-use=$ospart
# Disk partitioning information
part biosboot --fstype=biosboot --size=1 --ondisk=$boot --asprimary
part pv.20 --fstype="lvmpv" --ondisk=$ospart --size=$rootsize
part /boot --fstype="xfs" --ondisk=$boot --size=500
volgroup centos --pesize=4096 pv.20
#set ospart = $getVar('ospart', '20')
#set logpart = $getVar('logpart', '20')
#set libpart = $getVar('libpart', '60')
logvol /  --fstype="xfs" --grow --maxsize=51200 --percent=$ospart --name=root --vgname=centos
#set swapsize = $getVar('swapsize', '2048')
logvol swap  --fstype="swap" --size=$swapsize --name=swap01 --vgname=centos
logvol /var/log --fstype="xfs" --grow --percent=$logpart --name=log --vgname=centos
logvol /var/lib --fstype="xfs" --grow --percent=$libpart --name=lib --vgname=centos
%packages
@core
-biosdevname
chrony
%end
%post
mkdir -p /root/.ssh
chmod 700 /root/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC5tDwoXxQ2oqGmPkbYeaF2h7bbhzT60H6bYJzdbQ6t8blUHIePi7DGaZuJQkcy55gI+fo9nc48PHP4mq1bf+hVdbIP8tiXE9bJMmmeO7acm0TWPNC7sNADRo9pqj2KPzpmuPnsGds/RCGtycIG9UONryC+JONiiEQa7z6KWbMgwInuWK23NpQDwR2f/Gc91ToAdz1XqXFl6YL01l4eenPOGyBrei9Zk2MFy0LN1iU9b8OstCwBw140rnRbSdKVtS5mr5n9GoRa2hcN2IDwrkbhrvAoAgzFmGxBamD7LF/OC8+cueWxNhqOaE7bvGjO2P4QWWgnQwDyonnSDWUqVBnD dahoo@dahoo-N56JK" > /root/.ssh/authorized_keys
chmod 600 /root/.ssh/authorized_keys
%end
