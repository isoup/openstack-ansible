import shade
import sys
from datetime import datetime as time
import logging
import yaml
from fortiosclient.client import FortiosApiClient
import utils

logger = logging.getLogger('test')
formatter = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.INFO, format=formatter)

# Initialize and turn on debug logging
shade.simple_logging(debug=False)

# Initialize cloud
# Cloud configs are read with os-client-config
server_to_upgrade = sys.argv[1]
cloud = shade.openstack_cloud(cloud='fasqa')
config = yaml.load(open('config.yaml'))
upgrade_config = config.get(server_to_upgrade)
virtual_servers = upgrade_config['virtual-servers']
fortigate = config.get('fortigate')
api = [(fortigate['host'], 80, False)]
user = fortigate['user']
password = fortigate['pass']
fos = FortiosApiClient(api, user, password)
backends_config = {}
for virtual_server in virtual_servers:
    v_server = virtual_server['name']
    response = fos.request("GET_FIREWALL_VIP", name=v_server,
                           vdom=fortigate['vdom'])
    backends_config[v_server] = response['results'][0]['realservers']
filters = "[?metadata.type=='" + server_to_upgrade + "']"
servers_on_cloud = cloud.search_servers(filters=filters)
for virtual_server in virtual_servers:
    if len(servers_on_cloud) != len(backends_config[virtual_server['name']]):
        logger.error("cloud and backends on fgt for %(v_server)s mismatch",
                     {'v_server': virtual_server['name']})
        logger.error("there are %(on_cloud)d on cloud and %(on_fgt)d on fgt",
                     {'on_cloud': len(servers_on_cloud),
                      'on_fgt': len(backends_config[virtual_server['name']])})
        fos.request("LOGOUT")
        sys.exit(-1)

logger.info("Adding backends in new version")
new_server_count = 0
for server in servers_on_cloud:
    server_name = server['name']
    server_image = upgrade_config['image']
    server_flavor = upgrade_config['flavor']
    server_network = upgrade_config['network']
    server_key = upgrade_config['key']
    server_meta = {'type': server_to_upgrade}
    new_server = utils.create_server(cloud, server_name, image=server_image,
                                     flavor=server_flavor,
                                     network=server_network,
                                     key_name=server_key, config_drive=True,
                                     meta=server_meta, wait=True)
    if new_server is None:
        logger.error("launching server failed")
    else:
        for virtual_server in virtual_servers:
            logger.info("disable %(ip)s for %(v_server)s",
                        {'ip': server['private_v4'],
                         'v_server': virtual_server['name']})
            v_server_backend = backends_config[virtual_server['name']]
            backend_id = utils.get_backend_id(v_server_backend,
                                              server['private_v4'])
            fos.request("SET_FIREWALL_REAL_SERVER",
                        virt_server_name=virtual_server['name'],
                        vdom=fortigate['vdom'], name=backend_id,
                        status="disable")

            logger.info("Wait for Traffic on Old Version to Go Offline")
            utils.wait_for_session_offline(fos, virtual_servers,
                                           server['private_v4'])

            logger.info("deleting %(ip)s for %(v_server)s",
                        {'ip': server['private_v4'],
                         'v_server': virtual_server['name']})
            fos.request("DELETE_FIREWALL_REAL_SERVER",
                        virt_server_name=virtual_server['name'],
                        vdom=fortigate['vdom'], name=backend_id)

            logger.info("deleting server %(server)s",
                        {'server': server['name']})
            result = utils.delete_server(cloud, server['id'])
            if result is False:
                logger.error('deleting server %(ip)s failed',
                             {'ip': server['private_v4']})

            backend_config = {
                  "ip": new_server['private_v4'],
                  "port": virtual_server['backend-port'],
                  "status": "active"
            }
            logger.info('adding backend %(ip)s for %(v_server)s',
                        {'ip': new_server['private_v4'],
                         'v_server': virtual_server['name']})
            fos.request("ADD_FIREWALL_REAL_SERVER",
                        virt_server_name=virtual_server['name'],
                        vdom=fortigate['vdom'], **backend_config)
            new_server_count = new_server_count + 1

if new_server_count == 0:
    logger.error('No new servers are launched, abort upgrade')
    fos.request('LOGOUT')
    sys.exit(-1)
elif new_server_count < len(servers_on_cloud):
    logger.warn('There are fewer servers in new version')

fos.request("LOGOUT")

