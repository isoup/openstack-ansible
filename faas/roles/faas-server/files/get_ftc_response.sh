#!/bin/bash
#HOSTNAME="${COLLECTD_HOSTNAME:-localhost}"
HOSTNAME="faas-probe-sunnyvale"
#INTERVAL="${COLLECTD_INTERVAL:-30}"
INTERVAL=30

while sleep "$INTERVAL"; do
  res=`curl -o /dev/null -s -w '%{http_code} %{time_total}' https://ftc.fortinet.com/version?no_cache=true`
  res=($res)
  http_code=${res[0]}
  if [[ $http_code == 200 ]]; then
    VALUE=${res[1]}
  else
    VALUE=10.00
  fi
  VALUE=`echo "$VALUE * 1000" |bc -l`
  VALUE2=`mysql -ufas -pfas -h10.27.103.249 fas -s -e"select count(*) from authenticators where customer_id is null;"`
  VALUE3=`mysql -ufas -pfas -h10.27.103.249 fas -s -e"select count(*) from authenticators where customer_id is not null;"`
  echo "PUTVAL \"$HOSTNAME/exec/gauge-ftc_portal_response_time\" interval=$INTERVAL N:$VALUE"
  echo "PUTVAL \"$HOSTNAME/exec/gauge-ftc_fac_available\" interval=$INTERVAL N:$VALUE2"
  echo "PUTVAL \"$HOSTNAME/exec/gauge-ftc_fac_used\" interval=$INTERVAL N:$VALUE3"
done
