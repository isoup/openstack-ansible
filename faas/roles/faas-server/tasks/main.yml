- name: yum update and install required packages
  yum: name={{ item }} update_cache=yes state=installed
  with_items:
      - gcc
      - python-devel
      - mariadb-devel
      - git
      - epel-release
      - logrotate
  tags:
    - fas

- name: Install filebeat
  include: tasks/filebeat.yml
  tags:
    - fas
    - monitor
    - elastic
    - filebeat

- name: Add fas group
  group:
    name: "{{ fas_group }}"
    state: present
  tags:
    - fas

- name: Add fas user
  user:
    name: "{{ fas_user }}"
    groups: "{{ fas_group }}"
    shell: /bin/false
  tags:
    - fas

- name: Create fas folders
  file:
    path: "{{ item.path }}"
    mode: "{{ item.mode }}"
    state: directory
    owner: "{{ fas_user }}"
    group: "{{ fas_group }}"
  with_items:
    - {"path": "/var/log/fas", "mode": 755}
    - {"path": "/etc/fas", "mode": 755}
    - {"path": "/etc/fas/fernet-keys", "mode": 700}
    - {"path": "{{ fas_home_dir }}", "mode": 755}
  tags:
    - fas

- name: Copy ssl folder under /etc/fas
  copy:
    src: ssl
    dest: /etc/fas
    owner: "{{ fas_user }}"
    group: "{{ fas_group }}"
  tags:
    - fas

- name: Copy pip version cap file
  copy: src={{ item.src }} dest={{ item.dest }}
  with_items:
    - {src: 'cap.txt', dest: '/tmp/cap.txt'}
    - {src: 'get-pip.py', dest: '/tmp/get-pip.py'}
  tags:
    - fas

- name: Install latest pip
  command: 'python /tmp/get-pip.py -c /tmp/cap.txt'
  tags:
    - fas

- name: Copy tarball
  copy:
    src: "dist/{{ fas_package }}"
    dest: "{{ fas_home_dir }}"
  tags:
    - fas

- name: Install fac api client
  pip:
    name: git+https://github.com/samsu/api_client
  tags:
    - fas

- name: Install fas python package
  pip:
    name: "file://{{ fas_home_dir }}/{{ fas_package_name }}"
  register: pip_result
  notify:
    - restart fas
  tags:
    - fas

- name: Print pip install result
  debug: var=pip_result
  tags:
    - fas

- name: copy service file
  template: src=fas-server.service.j2 dest=/lib/systemd/system/fas-server.service mode=0644
  notify:
    - reload systemctl
    - restart fas
  tags:
    - fas

- name: copy fas server config
  template: src=fas.conf.j2 dest=/etc/fas/fas.conf owner={{ fas_user }} group={{ fas_group }}
  notify:
    - restart fas
  tags:
    - fas

- name: copy faas balance and faas usage service files
  template: src={{ item.name }} dest={{ item.dst }} mode={{ item.mode }}
  with_items:
    - { name : fas-balance.service.j2, dst: /lib/systemd/system/fas-balance.service, mode: 0644 }
    - { name : fas-usage.service.j2, dst: /lib/systemd/system/fas-usage.service, mode: 0644 }
  when:
    - fas_billing_node == true
  notify:
    - reload systemctl
    - restart fas-balance
    - restart fas-usage
  tags:
    - fas
    - balance
    - usage


- name: generate fernet keys
  command: fas-manage fernet_setup
  become: true
  become_user: "{{ fas_user }}"
  tags:
    - fas

- name: rotate fernet keys
  command: fas-manage fernet_rotate
  become: true
  become_user: "{{ fas_user }}"
  tags:
    - fas

- name: configure logrotate
  template:
    src: faas_rotate.conf.j2
    dest: /etc/logrotate.d/faas.conf
  tags:
    - fas

- name: start fas service
  service: name=fas-server.service state=started enabled=yes
  when:
    - build_only != true
    - fas_billing_node == false
  tags:
    - fas

- name: enable fas service
  service: name=fas-server.service state=stopped enabled=yes
  when:
    - build_only == true
    - fas_billing_node == false
  tags:
    - fas

- name: start fas balance and fas usage service
  service: name={{ item }}.service state=started enabled=yes
  with_items:
    - fas-balance
    - fas-usage
  when:
    - build_only != true
    - fas_billing_node == true
  tags:
    - fas
    - balance
    - usage

- name: enable fas balance and fas usage service
  service: name={{ item }}.service state=stopped enabled=yes
  when:
    - build_only == true
    - fas_billing_node == true
  with_items:
    - fas-balance
    - fas-usage
  tags:
    - fas
    - balance
    - usage

- name: configure collectd
  include: tasks/collectd.yml
  tags:
    - fas
    - monitor
    - collectd

- name: Sync to disk
  command: /bin/bash -c 'sync && sleep 5'
  tags:
    - fas
