HOSTNAME="${COLLECTD_HOSTNAME:-localhost}"
INTERVAL="${COLLECTD_INTERVAL:-60}"
 
while sleep "$INTERVAL"; do
  VALUE=`netstat -antu | grep {{ fas_port }} |grep SYN_RECV |wc -l`
  echo "PUTVAL \"$HOSTNAME/exec/gauge-tcp_syn_recv\" interval=$INTERVAL N:$VALUE"
done
