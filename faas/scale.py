import shade
import sys
from datetime import datetime as time
import yaml
from fortiosclient.client import FortiosApiClient
import time as systime
import utils
import logging
import sys

logger = logging.getLogger('test')
formatter = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.INFO, format=formatter)


# Initialize and turn on debug logging
shade.simple_logging(debug=False)

# Initialize cloud
# Cloud configs are read with os-client-config
server_to_scale = sys.argv[1]
strategy = sys.argv[2]
cloud = shade.openstack_cloud(cloud='fasqa')
config = yaml.load(open('config.yaml'))
scale_config = config.get(server_to_scale)
virtual_servers = scale_config['virtual-servers']
fortigate = config.get('fortigate')
api = [(fortigate['host'], 80, False)]
user = fortigate['user']
password = fortigate['pass']
fos = FortiosApiClient(api, user, password)
backends_config = {}
for virtual_server in virtual_servers:
    v_server_name = virtual_server['name']
    response = fos.request("GET_FIREWALL_VIP", name=v_server_name,
                           vdom=fortigate['vdom'])
    backends_config[v_server_name] = response['results'][0]['realservers']
filters = "[?metadata.type=='" + server_to_scale + "']"
servers_on_cloud = cloud.search_servers(filters=filters)
for virtual_server in virtual_servers:
    v_server_name = virtual_server['name']
    if len(servers_on_cloud) != len(backends_config[v_server_name]):
        logger.error("cloud and backends for %(v_server)s mismatch",
                     "on cloud:%(cloud_number)d; on fortigate:%(fgt_number)d",
                     {'cloud_number': len(servers_on_cloud),
                      'fgt_number': len(backends_config[v_server_name]),
                      'v_server': v_server_name})
        fos.request("LOGOUT")
        sys.exit(-1)

if scale_config['max-size'] > len(servers_on_cloud) and strategy == "up":
    logger.info('scaling up virtual servers')
    server_seq = str(len(servers_on_cloud) + 1).rjust(3, '0')
    server_name = server_to_scale + server_seq
    server_image = scale_config['image']
    server_flavor = scale_config['flavor']
    server_network = scale_config['network']
    server_key = scale_config['key']
    server_meta = {'type': server_to_scale}
    server = utils.create_server(cloud, server_name, image=server_image,
                                 flavor=server_flavor, network=server_network,
                                 key_name=server_key, config_drive=True,
                                 meta=server_meta, wait=True)
    if server is None:
        logger.error('launching server failed')
        fos.request('LOGOUT')
        sys.exit(-1)

    for virtual_server in virtual_servers:
        backend_config = {
              "ip": server['private_v4'],
              "port": virtual_server['backend-port'],
              "status": "active"
        }
        logger.info('++++++++++++++++++++adding server %(ip)s to %(virtual_server)s',
                    {'ip': server['private_v4'],
                     'virtual_server': virtual_server['name']})
        fos.request("ADD_FIREWALL_REAL_SERVER",
                    virt_server_name=virtual_server['name'],
                    vdom=fortigate['vdom'], **backend_config)
    fos.request("LOGOUT")
    sys.exit(0)

if scale_config['min-size'] < len(servers_on_cloud) and strategy == "down":
    server_seq = str(len(servers_on_cloud)).rjust(3, '0')
    server_name = server_to_scale + server_seq
    server_to_delete = utils.get_server_from_name(servers_on_cloud,
                                                  server_name)
    if server_to_delete is None:
        logger.error('server is out of sequence, cannot delete last one')
        fos.request('LOGOUT')
        sys.exit(-1)

    for virtual_server in virtual_servers:
        backend_config = backends_config[virtual_server['name']]
        backend_id = utils.get_backend_id(backend_config,
                                          server_to_delete['private_v4'])
        logger.info("disabling server %(ip)s for %(v_server)s",
                    {'ip': server_to_delete['private_v4'],
                     'v_server': virtual_server['name']})
        fos.request("SET_FIREWALL_REAL_SERVER",
                    virt_server_name=virtual_server['name'],
                    vdom=fortigate['vdom'], name=backend_id,
                    status="disable")
        logger.info("wait for sessions to get offline until 300s timeout")
        utils.wait_for_session_offline(fos, [virtual_server],
                                       server_to_delete['private_v4'],
                                       timeout=300)
        logger.info("deleting ip %(ip)s from %(v_server)s",
                    {'ip': server_to_delete['private_v4'],
                     'v_server': virtual_server['name']})
        fos.request('DELETE_FIREWALL_REAL_SERVER',
                    virt_server_name=virtual_server['name'],
                    vdom=fortigate['vdom'], name=backend_id)
    fos.request("LOGOUT")
    logger.info("deleting server %(name)s from cloud",
                {'name': server_to_delete['name']})
    result = utils.delete_server(cloud, server_to_delete['id'])
    if result is False:
        logger.error("deleting server failed for %(name)s",
                     {'name': server_to_delete['name']})
    sys.exit(0)

fos.request("LOGOUT")
logger.info("can't scale up or down because max/min has been reached,"
            "number of backends currently is %(servers_on_cloud)d",
            {'servers_on_cloud': len(servers_on_cloud)})
