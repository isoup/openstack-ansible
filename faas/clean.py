import shade

cloud = shade.openstack_cloud(cloud='fasqa')

#filters = {'vm_state': 'active'}
filters = "[?metadata.type=='faas-server' && vm_state!='active']"
for server in cloud.search_servers(filters=filters):
    cloud.delete_server(server['id'])
