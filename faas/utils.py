#!/usr/bin/env python
import shade
import time
import logging


logger = logging.getLogger('test')

def wait_for_session_offline(fos, virtual_server_list, real_server,
                             vdom="root", timeout=300, count=100):
    start_time = time.time()
    virtual_server_list = [v['name'] for v in virtual_server_list]
    while  time.time() - start_time < timeout:
        active_sessions = 0
        response = fos.request("GET_MONITOR_LOAD_BALANCE", vdom=vdom,
                               count=count)
        for stat in response['results']:
            if stat['virtual_server_name'] not in virtual_server_list:
                continue
            for real_server_stat in stat['list']:
                if real_server_stat['real_server'] != real_server:
                    continue
                active_sessions = (active_sessions +
                                   real_server_stat['active_sessions'])
        if active_sessions == 0:
            logger.info('no session on the server, going to delete backend')
            break
        else:
            logger.info('there are still sessions %(sessions)d,'
                        'wait 5s and check back',
                        {'sessions': active_sessions})
            time.sleep(5)

def get_backend_id(backend_config, backend_ip):
    for backend in backend_config:
        if backend['ip'] == backend_ip:
            return backend['id']

def get_server_from_name(servers, name):
    for server in servers:
        if server['name'] == name:
            return server
    return None

def create_server(cloud, server_name, **kwargs):
    server = None
    for i in range(0, 3):
        try:
            server = cloud.create_server(server_name, **kwargs)
        except shade.exc.OpenStackCloudException:
            pass
        if server is not None:
            return server
    return server

def delete_server(cloud, server_id):
    for i in range(0, 3):
        try:
            result = cloud.delete_server(server_id)
        except shade.exc.OpenStackCloudException:
            pass
        if result in (True, False):
            return True
    return False
