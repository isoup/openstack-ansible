import shade
import sys
from datetime import datetime as time
import yaml
import time as systime
import logging

logger = logging.getLogger('test')
formatter = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.INFO, format=formatter)


# Initialize and turn on debug logging
shade.simple_logging(debug=False)

# Initialize cloud
# Cloud configs are read with os-client-config
server_to_get = sys.argv[1]
cloud = shade.openstack_cloud(cloud='fasqa')
config = yaml.load(open('config.yaml'))
filters = "[?metadata.type=='" + server_to_get + "']"
servers_on_cloud = cloud.search_servers(filters=filters)
for server in servers_on_cloud:
    print server['private_v4']
