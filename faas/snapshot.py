import shade
import sys
from datetime import datetime as time

# Initialize and turn on debug logging
shade.simple_logging(debug=False)

# Initialize cloud
# Cloud configs are read with os-client-config
cloud_id = sys.argv[1]
server_name = sys.argv[2]
cloud = shade.openstack_cloud(cloud=cloud_id)
image_name = server_name + '_snapshot_' + time.utcnow().strftime("%Y_%m_%d_%H:%M:%S")
image = cloud.create_image_snapshot(image_name, server_name, wait=True)
if image:
    print image.id
else:
    print "snapshot failed"
